import 'package:flutter/material.dart';
import 'package:mail_app/screens/mail_screen.dart';

import '../models/models.dart';

class MailWidget extends StatelessWidget {
  const MailWidget({
    Key? key,
    required this.email, 
    required this.readEmail, 
    required this.deleteEmail,
  }) : super(key: key);

  final Email email;
  final Function readEmail;
  final Function deleteEmail;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details){
        deleteEmail(email.id);
      },
      child: InkWell(
        onLongPress: () => readEmail(email.id),
        onTap: (){
          readEmail(email.id);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MailScreen(email: email)),
          );
        },
        child: Row(
          children: [
             Expanded(
              flex: 2, // 20%
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children:  [
                   Center(
                    child: Container(
                      width: 10.0,
                      height: 10.0,
                      decoration: BoxDecoration(
                          color: (email.read) ? Colors.white : Colors.red, shape: BoxShape.circle
                        ),
                    ),
                  ),
                ]
              )
            ),
            Expanded(
              flex: 8, // 80%
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(email.dateTime.toString()),
                  Text(email.from.toString()),
                  Text(email.subject.toString())
                ],
              )
            ),
          ],
          ),
      ),
    );
  }
}
