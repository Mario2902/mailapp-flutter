import 'package:flutter/material.dart';
import 'package:mail_app/widgets/mail_widget.dart';

import '../models/models.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<Email> emails = Backend().getEmails();

  void readEmail(int emailId){
    Backend().markEmailAsRead(emailId);
    setState(() {
    });
  }
  
  void deleteEmail(int emailId){
    Backend().deleteEmail(emailId);
      setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Mailapp"),
        backgroundColor: Colors.indigo,
      ),
      body: ListView.separated(
        itemCount: emails.length,
        separatorBuilder: (_,__ ) => const Divider(
            thickness: 1,
            indent: 80,
            endIndent: 20,
            color: Colors.indigo,
        ),
        itemBuilder: (context, index) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: MailWidget(email: emails[index], readEmail: readEmail, deleteEmail: deleteEmail),
          ),
        ),
    );
  }
}


