import 'package:flutter/material.dart';

import 'package:mail_app/models/email.dart';

class MailScreen extends StatelessWidget {
   
  const MailScreen({Key? key, required this.email}) : super(key: key);
  
  final Email email;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(email.subject),
        backgroundColor: Colors.indigo,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("From: ${email.from}"),
            const Divider(
              thickness: 1,
              indent: 10,
              endIndent: 10,
              color: Colors.indigo,
            ),
      
            Text(email.subject),
            Text(email.dateTime.toString()),
            const Divider(
              thickness: 1,
              indent: 10,
              endIndent: 10,
              color: Colors.indigo,
            ),
      
            Text(email.body),
          ],
        ),
      )
    );
  }
}